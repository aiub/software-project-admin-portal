import React, { createContext, useState } from 'react';
import { GET_ALL_USERS_URL, CREATE_USERS_URL, GET_USER_BY_ID_URL, UPDATE_User_SHIPPING, UPDATE_User_VARIANT } from "../helpers/apis";
import axios from "axios";

export const UserContext = createContext();

const UserContextProvider = ({ children }) => {

    const [User, setUser] = useState(null);
    const [UserInfo, setUserInfo] = useState(null);


    const getAllUser = async () => {
        try {
            const res = await axios.get(GET_ALL_USERS_URL);
            setUser(res.data);
            return true;
        } catch (error) {
            console.log("User loading error.", error.response ? error.response.message : error.message)
            return false;
        }

    }


    const addUser = async (data) => {

        try {
            await axios.post(CREATE_USERS_URL, data);
            setUser([...User, data]);
            return true;
        } catch (error) {
            console.log("User post error.", error.response ? error.response.message : error.message);
            return false;
        }
    }

    const getUserById = async (id) => {

        try {
            const res = await axios.get(GET_USER_BY_ID_URL + id);
            setUserInfo(res.data);
            return true;
        } catch (error) {
            console.log("User additional info getting error.", error)
            return false;
        }
    }


    return (
        <UserContext.Provider
            value={{
                User,
                getAllUser,
                getUserById,
                addUser,
            }}
        >
            {children}
        </UserContext.Provider>
    );
}

export default UserContextProvider;