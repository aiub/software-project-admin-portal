import React, { createContext, useState } from 'react';
import { GET_SELLER_INFO } from "../helpers/apis";
import axios from "axios";

export const CustomerInfoContext = createContext();

const CustomerInfoContextProvider = ({ children }) => {

    const [sellerInfo, setSellerInfo] = useState([]);

    const [errorMsg, setErrorMsg] = useState('');
    const [loading, setLoading] = useState(false);
    const [successMsg, setSuccessMsg] = useState("");

    const getSellerInfo = () => {
        setLoading(true);
        axios.get(GET_SELLER_INFO + "5d91bde778785a2e35c77f8a")
            .then(res => {
                console.log(res.data);
                setSellerInfo(res.data);
                setLoading(false)
            })
            .catch(err => {
                setErrorMsg(err.response ? err.response.data : err.message)
                setLoading(false)
            });
    }

    const resetStatus = () => {
        setErrorMsg('');
        setSuccessMsg('');
        setLoading(false)
    }

    return (
        <CustomerInfoContext.Provider value={{
            sellerInfo,
            errorMsg,
            loading,
            successMsg,
            getSellerInfo,
            resetStatus
        }}
        >
            {children}
        </CustomerInfoContext.Provider>
    );

}

export default CustomerInfoContextProvider;