import React from 'react';
import { Icon } from "antd";
import * as PATH from '../routes/Slugs';

export const NavsForSeller = [
    {
        key: 'dashboard',
        title: 'Dashboard',
        path: DASHBOARD_PATH,
        icon: <Icon type="pie-chart" />,
        subMenu: null
    },
    {
        key: 'products',
        title: 'Products',
        path: PRODUCT_PATH,
        icon: <Icon type="shopping-cart" />,
        subMenu: null
    },
    {
        key: 'order',
        title: 'Order',
        path: ORDER_PATH,
        icon: <Icon type="pie-chart" />,
        subMenu: null
    },
    {
        key: 'user',
        title: 'USER',
        path: USER_PATH,
        icon: <Icon type="user" />,
        subMenu: null
    },
    {
        key: 'pages',
        title: 'Pages',
        icon: <Icon type="star" />,
        subMenu: [
            {
                key: 'user',
                title: 'User',
                path: PATH.USER_LIST_VIEW_PATH,
                icon: <Icon type="user" />,
                subMenu: null
            },
            {
                key: 'login',
                title: 'Login',
                path: LOGIN_PATH,
                icon: <Icon type="login" />,
                subMenu: null
            },
            {
                key: 'page403',
                title: 'Page403',
                path: PAGE_403_PATH,
            },
            {
                key: 'page404',
                title: 'Page404',
                path: PAGE_404_PATH,
            },
            {
                key: 'page500',
                title: 'Page500',
                path: PAGE_500_PATH,
            }
        ]
    }
]

export const NavsForCustomer = [
    {
        key: 'dashboard',
        title: 'Dashboard',
        path: DASHBOARD_PATH,
        icon: <Icon type="pie-chart" />,
        subMenu: null
    },
    {
        key: 'order',
        title: 'Order',
        path: ORDER_PATH,
        icon: <Icon type="pie-chart" />,
        subMenu: null
    },
    {
        key: 'profile',
        title: 'PROFILE',
        path: USER_PATH,
        icon: <Icon type="user" />,
        subMenu: null
    },
    {
        key: 'pages',
        title: 'Pages',
        icon: <Icon type="star" />,
        subMenu: [
            {
                key: 'login',
                title: 'Login',
                path: LOGIN_PATH,
                icon: <Icon type="login" />,
                subMenu: null
            },
            {
                key: 'page403',
                title: 'Page403',
                path: PAGE_403_PATH,
            },
            {
                key: 'page404',
                title: 'Page404',
                path: PAGE_404_PATH,
            },
            {
                key: 'page500',
                title: 'Page500',
                path: PAGE_500_PATH,
            }
        ]
    }
]
