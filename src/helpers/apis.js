
export const ROOT_URL = "http://localhost:3002";
export const VERSION = "v1";
export const API_URL = `${ROOT_URL}/api/${VERSION}`;

//login API
export const LOGIN_API_URL = `${API_URL}/login/`;
export const CHANGE_PASSWORD_API_URL = '';
export const UPDATE_PROFILE_API_URL = '';
export const GET_USER_PROFILE = `${API_URL}/user/profile`;

//product apis
export const PRODUCT_API_URL = `${API_URL}/products`
export const PRODUCT_FILE_UPLOAD = "/api/v1/files/upload/product";
export const GET_ALL_PRODUCT_API = `${PRODUCT_API_URL}`;
export const POST_PRODUCT_API = `${PRODUCT_API_URL}/post`;
export const GET_PRODUCT_ADDITIONAL_INFO = `${PRODUCT_API_URL}/additional-info/`;
export const UPDATE_PRODUCT_VARIANT = `${API_URL}/product-variant/update/`
export const UPDATE_PRODUCT_SHIPPING = `${API_URL}/shipping/update/`

//order apis
export const ORDER_API_URL = `${API_URL}/orders`;
export const GET_ALL_PRODUCTS = `${ORDER_API_URL}`;
export const GET_ALL_ORDERED_PRODUCT = `${ORDER_API_URL}/order-id/`;
export const GET_DELIVERYINFO = `${API_URL}/delivery-info/`;
export const GET_ORDER_STATUS = `${API_URL}/order-status/customer-id/`;

//customer apis
export const GET_SELLER_INFO = "/api/v1/seller-info/id/"


//User
export const GET_ALL_USERS_URL = `${PRIVATE_API_URL}/user`;
export const CREATE_USERS_URL = `${PRIVATE_API_URL}/user/create`;
export const GET_USER_BY_ID_URL = `${PRIVATE_API_URL}/user/id`; 
export const UPDATE_USER_BY_ID_URL = `${PRIVATE_API_URL}/user/update`;


//order apis

