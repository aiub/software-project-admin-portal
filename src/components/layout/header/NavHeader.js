import React, { useContext } from 'react';
import { Layout, Dropdown, Menu, Avatar, Row, Col } from 'antd';
import { Link } from 'react-router-dom';

/* SCSS */
import './nav_header.scss'
import { AuthContext } from '../../../contexts/AuthContextProvider';



const { Header } = Layout;

const NavHeader = ({ children }) => {

    const authContext = useContext(AuthContext);


    const logout = () => {
        authContext.logoutRequest();
    }

    const menu = (
        <Menu style={{ minWidth: "120px", backgroundColor: "#ffffff" }}>
            <Menu.Item key="0">
                <Link to="">Profile</Link>
            </Menu.Item>
            <Menu.Divider />
            <Menu.Item key="1" onClick={logout}>
                Logout
            </Menu.Item>
        </Menu>
    );



    return (
        <Header className="nav_header" >
            <Row style={{ width: "100%" }}>
                <Col span={12}>
                    <div className="logo" style={{ float: "left" }}>

                        {children}
                    </div>
                </Col>
                <Col span={12}>
                    <Dropdown className="drop_down" overlay={menu} trigger={['hover']} >
                        <div style={{ float: "right" }}>
                            <span>admin@ecourier.org</span> &nbsp;
                        <Avatar size="large" icon="user" className="ant-dropdown-link" />
                        </div>
                    </Dropdown>
                </Col>
            </Row>
        </Header>
    );
}

export default NavHeader;