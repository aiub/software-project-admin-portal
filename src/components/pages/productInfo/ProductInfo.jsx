import React, { Fragment, useContext, useEffect, useState } from "react";
import PageWrapper from '../../common/PageWrapper';
import LoadingSuspense from "../../common/LoadingSuspense";
import { PageHeader, Button, Input, Table, Icon } from 'antd';
import { PRODUCT_PATH, PRODUCT_INFO_PATH, EDIT_PATH } from "../../../routes/Slugs";
import { productFormData } from "../../pages/addProduct/newProductFormData";
import { Link, Redirect } from 'react-router-dom';
import { ProductContext } from "../../../contexts/ProductContextProvider";
import { ROOT_URL } from "../../../helpers/apis";

const routes = [
    {
        path: PRODUCT_PATH,
        breadcrumbName: 'Product List',
    },
    {
        path: PRODUCT_INFO_PATH,
        breadcrumbName: 'Product Information',
    }
]

export default (props) => {

    const productContext = useContext(ProductContext);
    const [loading, setLoading] = useState(false);
    const [errorMsg, setErrorMsg] = useState('');

    const data = props.location.data;

    async function getProductAdditionalInfo(id) {
        setLoading(true);
        const status = await productContext.getProductAdditionalInfo(id);
        if (!status) setErrorMsg('Product Additional information loading error ! Please check the console or contact with PachPai.');

        setLoading(false);
    }

    useEffect(() => {
        data && getProductAdditionalInfo(data._id);
    }, []);

    const productInfo = productContext.productInfo;

    const pageHeader = (data || productInfo) && <PageHeader title={"Product Detail Information"} subTitle={`Id: ${productInfo && productInfo.product._id}`}
        breadcrumb={{ routes }}
    />
    return (
        data || productInfo ?
            <PageWrapper pageHeader={pageHeader}>
                {
                    productInfo ?
                        <Fragment>

                            {/* Render variants table */}
                            <div className="internal_box">
                                <h4 className="internal_box_header">Variants</h4>
                                <Table
                                    className="internal_table"
                                    columns={variantColumns}
                                    dataSource={productInfo.variants.map((e, i) => ({ ...e, key: i }))}

                                    // render package prices of the variant.
                                    // expandedRowRender={(e) => getInternalTable(packagePricingColumns, e.packagePricing, "Package Prices")}
                                    pagination={false}
                                />
                            </div>

                            {/* Render shipping information */}
                            <div className="internal_box">
                                <h4 className="internal_box_header">Shipping</h4>
                                <Table className="internal_table"
                                    columns={shippingColumns}
                                    dataSource={[{ ...productInfo.product.shipping, key: "shipping" }]}
                                    pagination={false}
                                />
                            </div>

                            {/* Render tags */}
                            <div className="internal_box">
                                <h4 className="internal_box_header">Tags</h4>
                                <div className="internal_table">
                                    {
                                        productInfo.tags.map((e, i) => <span key={i} className="tag_box">{e.tagId.value}<Button className="button" type="link"><Icon type="close" /></Button></span>)
                                    }
                                    <div className="add_tag_box">
                                        <Input placeholder="add more tags" allowClear />
                                        <Button>Add tag</Button>
                                    </div>
                                </div>
                            </div>

                        </Fragment> : <LoadingSuspense />
                }

            </PageWrapper> : <Redirect to={PRODUCT_PATH} />
    )
}


const getInternalTable = (columns, data, header) => {
    return (
        <div className="internal_box">
            <h4 className="internal_box_header">{header}</h4>
            <Table className="internal_table" columns={columns} dataSource={data.map((e, i) => ({ ...e, key: i }))} pagination={false} />
        </div>
    )
}

// Define columns for tables.
const shippingColumns = [
    {
        title: "Height",
        dataIndex: "height",
        key: "height",
    },
    {
        title: "Width",
        dataIndex: "width",
        key: "width"
    },
    {
        title: "Length",
        dataIndex: "length",
        key: "length"
    },
    {
        title: "Weight",
        dataIndex: "weight",
        key: "weight"
    },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: (e) => (
            <Fragment>
                <Link to={{ pathname: PRODUCT_INFO_PATH, data: e }}>
                    <Icon type="eye" />
                </Link>
                &nbsp;&nbsp;

                    <Link to={{ pathname: EDIT_PATH, data: e, formData: productFormData.shipping.config.value, header: "Edit Shipping Information" }}>
                    <Icon type="edit" />
                </Link>&nbsp;&nbsp;
                    <a href="#"><Icon type="delete" /></a>
            </Fragment>)
    },
]

const variantColumns = [

    {
        title: 'Images',
        dataIndex: 'images',
        key: 'images',
        render: e => <img style = {{height: '70px', width: '70px'}} src = {`${ROOT_URL}${e[0]}`}/>
    },
    {
        title: 'Color',
        dataIndex: 'attrs',
        key: 'color',
        render: e => e.colorFamily
    },
    {
        title: 'Size',
        dataIndex: 'attrs',
        key: 'color',
        render: e => e.size
    },
    {
        title: 'Action',
        dataIndex: '',
        key: 'x',
        render: (e) => (
            <Fragment>
                <Link to={{ pathname: PRODUCT_INFO_PATH }}>
                    <Icon type="eye" />
                </Link>
                &nbsp;&nbsp;

                    <Link to={{ pathname: EDIT_PATH, data: e, formData: productFormData.variants.config.value, header: "Edit Variant Information" }}><Icon type="edit" /></Link>&nbsp;&nbsp;
                    <a href="#"><Icon type="delete" /></a>
            </Fragment>)
    }

];

const packagePricingColumns = [
    {
        title: "Quantity",
        dataIndex: "piece",
        key: "quantity",
    },
    {
        title: "Price",
        dataIndex: "price",
        key: "price",
    },

]


// const updateVariant=(id)=> {
//         const productContext = useContext(ProductContext);
//         productContext.updateProductVariant(id);
//     }