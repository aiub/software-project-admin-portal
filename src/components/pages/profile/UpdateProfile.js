import {PageHeader} from "antd";
import React, {useContext, useState} from "react";
import PageWrapper from "../../common/PageWrapper";
import ConfirmationModal from "../../common/ConfirmationModal";
import ErrorModal from "../../common/ErrorModal";
import "../../../assets/scss/style.scss";
import AuthService from "../../../services/AuthService";
import {AuthContext} from "../../../contexts/AuthContextProvider";
import ProfileForm from "./ProfileFrom";

const UpdateProfile = (props) => {

    const [loadingSubmit, setLoadingSubmit] = useState(false);
    const authContext = useContext(AuthContext);

    // const id = props.match.params.id;

    const onSubmit = async (values) => {
        try {
            setLoadingSubmit(true);
            const response = await AuthService.updateProfile(values);
            ConfirmationModal('Profile updated successfully', 'success');
            setLoadingSubmit(false);
        } catch (error) {
            setLoadingSubmit(false);
            ErrorModal(error);
        }
    }


    const pageHeader = <PageHeader
        title="Edit Profile"
        subTitle="edit your profile information"/>

    return (
        <PageWrapper pageHeader={pageHeader}>
            {<ProfileForm reseller={authContext.profile} onSubmit={onSubmit} loadingSubmit={loadingSubmit}/>}
        </PageWrapper>
    )
}

export default UpdateProfile;