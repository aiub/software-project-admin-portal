import {Button, Form, Input, PageHeader} from "antd";
import React, {useContext, useEffect, useState} from "react";
import PageWrapper from "../../common/PageWrapper";
import LoadingSuspense from '../../common/LoadingSuspense';
import "../../../assets/scss/style.scss";
import ObjectViewer from "../../common/ObjectViewer";
import {AuthContext} from "../../../contexts/AuthContextProvider";
import {Link} from "react-router-dom";
import {PROFILE_UPDATE_PATH} from "../../../routes/Slugs";
import ModalButton from "../../common/ModalButton";
import AuthService from "../../../services/AuthService";
import ConfirmationModal from "../../common/ConfirmationModal";
import ErrorModal from "../../common/ErrorModal";
import * as Permission from "../../../helpers/Permissions";


export default (props) => {

    const authContext = useContext(AuthContext);
    const [profile, setProfile] = useState(null);
    const [loadingSubmit, setLoadingSubmit] = useState(false);
    const [role, setRole] = useState(null)

    useEffect(() => {
        if (authContext.profile) {
            let _profile = {...authContext.profile};

            _profile.firstName = _profile.name.firstName;
            _profile.lastName = _profile.name.lastName;
            _profile.fullName = _profile.name.fullName;
            delete _profile['name'];

            setRole(_profile.role);

            delete _profile.role;

            setProfile(_profile);
        }

    }, [authContext.loadingProfile]);

    const onSubmit = async (values) => {
        try {
            setLoadingSubmit(true);
            const response = await AuthService.changePassword(values);
            ConfirmationModal('Password changed successfully', 'success');
            setLoadingSubmit(false);
        } catch (error) {
            setLoadingSubmit(false);
            ErrorModal(error);
        }

    }

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };


    const pageHeader = <PageHeader
        title="Profile"
        subTitle={''}
        extra={
            [
                <ModalButton buttonText="Change password" okText='Ok' title="Change your password" key='changePass'>

                    <hr/>
                    <Form
                        // {...layout}
                        layout='vertical'
                        name="changePassword"
                        initialValues={{oldPassword: '', newPassword: ''}}
                        onFinish={(value) => onSubmit(value)}
                        onFinishFailed={onFinishFailed}
                        className="form"
                    >

                        <Form.Item
                            label="Old password"
                            name="oldPassword"
                            rules={[
                                {required: true, message: 'Please input your old password!'},
                                {min: 6, message: 'Password should be at least 6 character long.'}
                            ]}
                        >
                            <Input.Password/>
                        </Form.Item>

                        <Form.Item
                            label="New password"
                            name="newPassword"
                            rules={[
                                {required: true, message: 'Please input your new password!'},
                                {min: 6, message: 'Password should be at least 6 character long.'}
                            ]}
                        >
                            <Input.Password/>
                        </Form.Item>

                        <Form.Item>

                            {
                                loadingSubmit ? <LoadingSuspense/> :
                                    <Button type="primary" htmlType="submit">
                                        Submit
                                    </Button>
                            }


                        </Form.Item>


                    </Form>
                </ModalButton>,

                authContext.permissions.includes(Permission.MODIFY_USER_PROFILE) &&
                <Link to={PROFILE_UPDATE_PATH} key='update_profile'>
                    <Button type='primary'>
                        Edit profile
                    </Button></Link>

            ]
        }
    />

    return (
        <PageWrapper pageHeader={pageHeader}>
            {
                authContext.loadingProfile ? <LoadingSuspense/> :
                    authContext.profileErrorMsg ? <h1 className='big_error_msg'>{authContext.profileErrorMsg}</h1> :
                        <div>
                            {ObjectViewer(profile, 'Profile information')}
                            {ObjectViewer(role, 'Role')}
                        </div>
            }
        </PageWrapper>
    )
}