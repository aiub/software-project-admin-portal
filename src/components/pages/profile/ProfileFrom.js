import React from 'react';
import {Button, Form, Input, InputNumber} from 'antd';
import '../../../assets/scss/style.scss';
import LoadingSuspense from '../../common/LoadingSuspense';


const ProfileForm = (props) => {

    let {reseller, onSubmit, loadingSubmit} = props;

    reseller = reseller ? {...reseller, firstName: reseller.name.firstName, lastName: reseller.name.lastName} : {};

    const onFinish = values => {
        values.name = {
            firstName: values.firstName,
            lastName: values.lastName
        }
        onSubmit(values);
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };

    return (
        <div className="form_wrapper">
            <Form
                layout='vertical'
                name="reseller"
                initialValues={reseller}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                className="form"
            >

                <Form.Item
                    label="First name"
                    name="firstName"
                    rules={[{required: true, message: 'Please input first name'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Last name"
                    name="lastName"
                    rules={[{required: true, message: 'Please input last name'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Login"
                    name="login"
                    rules={[{required: true, message: 'Please input your login'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[{type: "email", message: 'Input a valid email'}]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Address"
                    name="address"
                >
                    <Input.TextArea/>
                </Form.Item>

                <Form.Item
                    label="ZipCode"
                    name="zipCode"
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Company name"
                    name="company"
                >
                    <Input/>
                </Form.Item>


                <Form.Item
                    label="Designation"
                    name="designation"
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Fax"
                    name="fax"
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Monthly pin creation limit"
                    name="pinCreationLimit"
                    rules={[
                        {required: true, message: 'Please input your pin creation limit'}
                    ]}
                >
                    <InputNumber/>
                </Form.Item>

                <Form.Item>
                    {
                        loadingSubmit ? <LoadingSuspense/> :
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                    }

                </Form.Item>
            </Form>
        </div>
    );
}

export default ProfileForm;
