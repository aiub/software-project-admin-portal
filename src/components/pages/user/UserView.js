import {Button, Form, Input, PageHeader} from "antd";
import React, {useEffect, useState} from "react";
import PageWrapper from "../../common/PageWrapper";
import UserService from "../../../services/UserService";
import LoadingSuspense from '../../common/LoadingSuspense';
import ErrorModal from "../../common/ErrorModal";
import "../../../assets/scss/style.scss";
import ObjectViewer from "../../common/ObjectViewer";
import ConfirmationModal from "../../common/ConfirmationModal";
import ModalButton from "../../common/ModalButton";

export default (props) => {

    const [user, setUser] = useState(null);
    const [errorMsg, setErrorMsg] = useState('');
    const [loading, setLoading] = useState(true);
    const [loadingSubmit, setLoadingSubmit] = useState(false);

    const id = props.match.params.id;

    useEffect(() => {
        getUserById();
    }, []);

    const getUserById = async () => {
        setLoading(true);
        try {
            const response = await UserService.getUserById(id);
            setUser(response.data);
            setLoading(false);
        } catch (error) {
            const message = error.response ? error.response.data.message : error.message;
            setErrorMsg(message);
            ErrorModal(error);
            setLoading(false);
        }
    }

    const onSubmit = async (values) => {
        try {
            setLoadingSubmit(true);
            const response = await UserService.changePassword(values, id);
            ConfirmationModal('Password changed successfully', 'success');
            setLoadingSubmit(false);
        } catch (error) {
            setLoadingSubmit(false);
            ErrorModal(error);
        }

    }

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };


    const pageHeader = <PageHeader
        title="User"
        subTitle={`ID: ${id}`} />
       

    return (
        <PageWrapper pageHeader={pageHeader}>
            {loading ? <LoadingSuspense/> : errorMsg ? <h1 className='big_error_msg'>{errorMsg}</h1> :
                <div>
                    {ObjectViewer({
                        ...user,
                        email: user.email,
                        active:user.active,
                        isSuperUser:user.isSuperUser,
                        isAdmin:user.isAdmin,
                        isStaff:user.isStaff,
                        birthDate: user.birthDate,
                        createdAt: user.createdAt,
                        updatedAt: user.updatedAt
                    }, 'User information')}

                    

                </div>}
        </PageWrapper>
    )
}