import {PageHeader} from "antd";
import React, {useEffect, useState} from "react";
import PageWrapper from "../../common/PageWrapper";
import UserForm from "./UserForm";
import UserService from "../../../services/UserService";
import ConfirmationModal from "../../common/ConfirmationModal";
import LoadingSuspense from '../../common/LoadingSuspense';
import ErrorModal from "../../common/ErrorModal";
import "../../../assets/scss/style.scss";

export default (props) => {

    const [user, setUser] = useState(null);
    const [errorMsg, setErrorMsg] = useState('');
    const [loading, setLoading] = useState(true);

    const [loadingSubmit, setLoadingSubmit] = useState(false);

    const id = props.match.params.id;

    useEffect(() => {
        getUserById();
    }, []);

    const getUserById = async () => {
        setLoading(true);
        try {
            const response = await UserService.getUserById(id);
            setUser(response.data);
            setLoading(false);
        } catch (error) {
            const message = error.response ? error.response.data.message : error.message;
            setErrorMsg(message);
            ErrorModal(error);
            setLoading(false);
        }
    }

    const onSubmit = async (values) => {
        try {
            setLoadingSubmit(true);
            const response = await UserService.updateUser(values, id);
            ConfirmationModal('User updated successfully', 'success');
            setLoadingSubmit(false);
        } catch (error) {
            setLoadingSubmit(false);
            ErrorModal(error);
        }
    }


    const pageHeader = <PageHeader
        title="Edit User"
        subTitle="Edit this User"/>

    return (
        <PageWrapper pageHeader={pageHeader}>
            {loading ? <LoadingSuspense/> : errorMsg ? <h1 className='big_error_msg'>{errorMsg}</h1> :
                <UserForm user={user} onSubmit={onSubmit} loadingSubmit={loadingSubmit}/>}
        </PageWrapper>
    )
}