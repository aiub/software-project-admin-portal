import {PageHeader} from "antd"
import React, {useState} from "react"
import PageWrapper from "../../common/PageWrapper"
import UserService from "../../../services/UserService";
import UserForm from "./UserForm"
import ErrorModal from "../../common/ErrorModal";
import ConfirmationModal from "../../common/ConfirmationModal";

export default () => {

    const [loadingSubmit, setLoadingSubmit] = useState(false);

    const onSubmit = async (values) => {
        try {
            setLoadingSubmit(true);
            const response = await UserService.createUser(values);
            ConfirmationModal('User create successfully', 'success');
            setLoadingSubmit(false);
        } catch (error) {
            setLoadingSubmit(false);
            ErrorModal(error);
        }
    }

    const pageHeader = <PageHeader
        title="Add User"
        subTitle="Add a new User"/>

    return (
        <PageWrapper pageHeader={pageHeader}>
            <UserForm onSubmit={onSubmit} loadingSubmit={loadingSubmit}/>
        </PageWrapper>
    )
}