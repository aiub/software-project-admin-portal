import React, {useContext, useEffect} from 'react';
import {Button, DatePicker, Form, Input, Select,Checkbox} from 'antd';
import '../../../assets/scss/style.scss';
import LoadingSuspense from '../../common/LoadingSuspense';
import {RoleContext} from '../../../contexts/RoleContextProvider';

import moment from 'moment';

const {Option} = Select;


export default (props) => {

    const roleContext = useContext(RoleContext);
    const formRef = React.createRef();

    useEffect(() => {
        roleContext.getAllRoles();
    }, []);

    const onSearchRole = value => {
        roleContext.getAllRoles({alias: value});
    }

    let {user, onSubmit, loadingSubmit} = props;

    const dateFormat = 'DD/MM/YYYY';

    user = user ? {
        ...user,
        password: user.password,
        email: user.email,
        active: user.active,
        isSuperUser: user.isSuperUser,
        isAdmin: user.isAdmin,
        isStaff: user.isStaff,
        birthDate: user.birthDate ? moment(user.birthDate, dateFormat) : null,
        createdAt: user.createdAt ? moment(user.createdAt, dateFormat) : null,
        updatedAt: user.updatedAt ? moment(user.updatedAt, dateFormat) : null,
    } : {};

    const onFinish = values => {
        values.role = roleContext.roles.find(e => e.alias === values.role);
        
    
    // Pending .... More to code here
        values.birthDate = values.birthDate.format('DD-MM-YYYY');
        values.createdAt = values.createdAt.format('DD-MM-YYYY');
        values.updatedAt = values.updatedAt.format('DD-MM-YYYY');

        onSubmit(values);
    };

    const onFinishFailed = errorInfo => {
        console.log('Failed:', errorInfo);
    };


    return (
        <div className="form_wrapper">
            <Form
                // {...layout}
                ref={formRef}
                layout='vertical'
                name="user"
                initialValues={user}
                onFinish={onFinish}
                onFinishFailed={onFinishFailed}
                className="form"
            >

                {!props.user &&
                <Form.Item
                    label="Password"
                    name="password"
                    rules={[
                        {required: true, message: 'Please input the password!'},
                        {min: 6, message: 'Minimum 6'}
                    ]}
                >
                    <Input/>
                </Form.Item>}

                <Form.Item
                    label="Email"
                    name="email"
                    rules={[
                        {required: true, message: 'Please input the email!'},
                        { message: 'Please enter a valid email address"'}
                    ]}
                >
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Active"
                    valuePropName="checked"
                    name="active"
                    value={true}

                >   <Checkbox />
                    <Input/>
                </Form.Item>
                
                <Form.Item
                    label="Super User"
                    valuePropName="checked"
                    name="isSuperUser"
                    value={true}

                >   <Checkbox />
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Admin"
                    valuePropName="checked"
                    name="isAdmin"
                    value={true}

                >   <Checkbox />
                    <Input/>
                </Form.Item>

                <Form.Item
                    label="Staff"
                    valuePropName="checked"
                    name="isStaff"
                    value={true}

                >   <Checkbox />
                    <Input/>
                </Form.Item>

                <Form.Item name="birthDate" label="Date of birth">
                    <DatePicker
                        defaultValue = { user.birthDate } 
                        format={dateFormat}
                        onChange = {value => formRef.current.setFieldsValue({birthDate: value})}
                    />
                </Form.Item>
                <Form.Item name="createdAt" label="Date of birth">
                    <DatePicker
                        defaultValue = { user.createdAt } 
                        format={dateFormat}
                        onChange = {value => formRef.current.setFieldsValue({createdAt: value})}
                    />
                </Form.Item>
                <Form.Item name="updatedAt" label="Date of birth">
                    <DatePicker
                        defaultValue = { user.updatedAt } 
                        format={dateFormat}
                        onChange = {value => formRef.current.setFieldsValue({updatedAt: value})}
                    />
                </Form.Item>

                <Form.Item>
                    {
                        loadingSubmit ? <LoadingSuspense/> :
                            <Button type="primary" htmlType="submit">
                                Submit
                            </Button>
                    }

                </Form.Item>
            </Form>
        </div>
    );
}
