import {EditOutlined, EyeOutlined} from '@ant-design/icons';
import {Button, Col, Collapse, Input, PageHeader, Row, Table} from 'antd';
import React, {useContext, useEffect, useState} from 'react';
import {Link, useHistory} from 'react-router-dom';
import {UserContext} from '../../../contexts/UserContextProvider';
import {ADD_USER_PATH, EDIT_USER_PATH, USER_LIST_VIEW_PATH, USER_VIEW_PATH} from '../../../routes/Slugs';
import LoadingSuspense from '../../common/LoadingSuspense';
import PageWrapper from '../../common/PageWrapper';
import "../../../assets/scss/style.scss";
import {bindUrlWithParams, getParams, useQuery} from '../../../helpers/Utils';
import {AuthContext} from '../../../contexts/AuthContextProvider';
import * as Permission from "../../../helpers/Permissions";

export default () => {

    const query = useQuery();
    const history = useHistory();
    const authContext = useContext(AuthContext);

    const userContext = useContext(UserContext);
    const [pagination, setPagination] = useState({
        total: 0,
        current: 1,
        pageSize: 10,
        showSizeChanger: true,
        showQuickJumper: true,
        pageSizeOptions: ["10", "30", "50", "100"]
    });
    const [search, setSearch] = useState({name: ''});


    useEffect(() => {

        const page = parseInt(query.get("page")) || 1;
        const size = parseInt(query.get("size")) || 10;

        const data = getParams(query, search);

        getUserList(page - 1, size, {...data});

        setPagination({
            ...pagination,
            current: page,
            pageSize: size,
            total: userContext.totalElements
        });

        setSearch({...search, ...data})

    }, []);

    useEffect(() => {

        const page = parseInt(query.get("page")) || 1;
        const size = parseInt(query.get("size")) || 10;


        setPagination({
            ...pagination,
            current: page,
            pageSize: size,
            total: userContext.totalElements
        });

    }, [userContext.totalElements]);

    const getUserList = async (page, size, search) => {
        await userContext.getUserList({page, size, ...search});
    }


    const handleTableChange = (pagination_) => {
        setPagination({...pagination, pageSize: pagination.pageSize, current: pagination_.current});
        getUserList(pagination_.current - 1, pagination_.pageSize, search);
        history.push(bindUrlWithParams(USER_LIST_VIEW_PATH, {
            page: pagination_.current,
            size: pagination.pageSize, ...search
        }));
    }

    const handleSearchInput = (event) => {
        const {name, value} = event.target;
        setSearch({...search, [name]: value});
    }

    const resetSearch = () => {
        const _search = {name: ''};
        setSearch(_search);
        setPagination({...pagination, pageSize: pagination.pageSize, current: 1});
        getUserList(0, 10, _search);
        history.push(bindUrlWithParams(USER_LIST_VIEW_PATH, {page: 1, size: pagination.pageSize, ..._search}));
    }

    const searchHandle = () => {
        const page = 1;
        setPagination({...pagination, current: page, pageSize: pagination.pageSize});
        getUserList(page - 1, pagination.pageSize, search);
        history.push(bindUrlWithParams(USER_LIST_VIEW_PATH, {page, size: pagination.pageSize, ...search}));
    }


    const columns = useColumns(authContext);

    const pageHeader = <PageHeader
        title="User list"
        subTitle="All users"
        extra={authContext.permissions.includes(Permission.CREATE_USER) && <Link key={0} to={ADD_USER_PATH}>
            <Button type='primary'>
                Add user
            </Button>
        </Link>}
    />


    return (
        <PageWrapper
            pageHeader={pageHeader}
        >
            <>
                <Collapse style={{marginBottom: "10px"}} bordered={true}
                          defaultActiveKey={["1"]}>
                    <h3 style={{float: 'right', marginRight: '20px'}}>Users : {userContext.totalElements}</h3>
                    <Collapse.Panel header="Search" key="1">
                        <Row style={{lineHeight: "1.5rem"}} gutter={32}>


                            <Col sm={24} md={6} xs={25}>
                                User Name <Input placeholder="Email Address"
                                                 name="email"
                                                 value={search.email}
                                                 onChange={handleSearchInput}
                                                 onPressEnter={() => searchHandle()}
                            />
                            </Col>

                            

                            <Col span={24} xs={25} style={{marginTop: "10px"}}>
                                <Button type={"primary"} onClick={() => searchHandle()}>Search</Button>
                                <Button
                                    style={{marginLeft: "10px"}}
                                    onClick={resetSearch}
                                    type="danger" ghost
                                >
                                    Reset
                                </Button>
                            </Col>
                        </Row>
                    </Collapse.Panel>

                </Collapse>
                {userContext.loadingUserList ? <LoadingSuspense/> :
                    userContext.userListErrorMsg ? <h2 className="big_error_msg">{userContext.userListErrorMsg}</h2> :
                        <Table columns={columns} dataSource={userContext.userList} key="userTable"
                               rowKey={row => row.id} pagination={pagination} onChange={handleTableChange}
                               scroll={{x: 1200, y: 200}}
                        />
                }
            </>
        </PageWrapper>
    )
}

const useColumns = (authContext) => {
    return [
        {
            title: 'Email',
            dataIndex: 'email',
            key: "email",
            width: 150
        },
        {
            title: 'Active',
            dataIndex: 'active',
            key: "active",
            width: 150
        },
        {
            title: 'Super User',
            dataIndex: 'isSuperUser',
            key: "isSuperUser",
            width: 150
        },
        {
            title: 'Admin',
            dataIndex: 'admin',
            key: "admin",
            width: 150
        },
        {
            title: 'Staff',
            dataIndex: 'isStaff',
            key: "isStaff",
            width: 150
        },
        {
            title: 'Birth Date',
            dataIndex: 'birthDate',
            key: "birthDate",
            width: 150
        },
        {
            title: 'Created At',
            dataIndex: 'createdAt',
            key: "createdAt",
            width: 150
        },
        {
            title: 'Updated At',
            dataIndex: 'updatedAt',
            key: "updatedAt",
            width: 150
        },

        {
            title: "...",
            dataIndex: "",
            key: "x",
            width: 100,
            fixed: 'right',
            render: e => (
                <>
                    <Link to={
                        {
                            pathname: `${USER_VIEW_PATH}/${e.id}`,
                            data: e,
                        }}>
                        <EyeOutlined/>
                    </Link>
                    &nbsp;&nbsp;
                    {authContext.permissions.includes(Permission.MODIFY_USER) && <Link to={
                        {
                            pathname: `${EDIT_USER_PATH}/${e.id}`,
                            data: e,
                        }}>
                        <EditOutlined/>
                    </Link>}
                </>
            )
        }
    ]

}