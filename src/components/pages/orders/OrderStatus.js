import React from "react";
import { Grid } from "@material-ui/core";
import styles from "./order.module.css"

export default () => {
    return (
        <Grid item xs={12} sm={12} md={12} className={styles.orderStatusRoot} >
            <Grid item xs={12} sm={12} md={12}>
                <p className={styles.OrderStatus}>Order Status</p>
            </Grid>
            <Grid container spacing={2}>
                <Grid item xs={6} sm={3} md={4} >
                    <SmallCard Infotype={"Total Sale"} amount={56} />
                </Grid>

                <Grid item xs={6} sm={3} md={4} >
                    <SmallCard Infotype={"total Buy"} amount={56} />
                </Grid>


                {/*{orderStatusArray.map((e, i) =>
                    <Grid item xs={6} sm={3} md={4} >
                        <SmallCard Infotype={e.key} amount={e.value} />
                </Grid>)}*/}
            </Grid>
        </Grid>
    )
}

function SmallCard(props) {
    return (
        <div className={styles.estimation} >
            <p className={styles.leftEstimation}>{props.Infotype}</p>
            <p className={styles.rightEstimation}>{props.amount}</p>
        </div>
    )
}