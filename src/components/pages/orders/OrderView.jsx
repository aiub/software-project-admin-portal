import React, { lazy, Suspense } from "react";
import PageWrapper from '../../common/PageWrapper';
import { PageHeader } from 'antd';
import OrderList from "./OrderList";
import LoadingSuspense from '../../common/LoadingSuspense';

const OrderStatus = lazy(() => import("./OrderStatus"));


const Products = () => {

    const pageHeader = <PageHeader title="Orders" subTitle="Your order list." />
    return (
        <PageWrapper pageHeader={pageHeader}>
            <Suspense fallback={<LoadingSuspense height="100vh" />}>
                <OrderStatus />
            </Suspense>
            <OrderList />
        </PageWrapper>
    )
}


export default Products;