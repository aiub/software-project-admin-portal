import React, { useState, useEffect, useContext, Fragment } from "react";
import { Table, Input, Button, Icon, Switch } from 'antd';
import Highlighter from 'react-highlight-words';
import { Link } from "react-router-dom";
import { ORDER_INFO_PATH } from "../../../routes/Slugs";
import { OrderContext } from "../../../contexts/OrderContextProvider";

const Orders = () => {

    const { columns } = useColumnWithSearch();
    const orderContext = useContext(OrderContext);

    useEffect(() => {
        orderContext.getAllOrders();
    }, [])

    return (
        <Fragment>
            {orderContext.errorMsg && <h2 style={{ color: "brown" }}>{orderContext.errorMsg}</h2>}

            <Table
                className="product_table"
                columns={columns}
                rowSelection={rowSelection}
                dataSource={orderContext.orders.map((e, i) => ({ ...e, key: i }))}
            />
        </Fragment>
    )
}

const useColumnWithSearch = () => {
    const [searchText, setSearchText] = useState('');

    const getColumnSearchProps = dataIndex => ({
        filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
            <div style={{ padding: 8 }}>
                <Input
                    placeholder={`Search ${dataIndex}`}
                    value={selectedKeys[0]}
                    onChange={e => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                    onPressEnter={() => handleSearch(selectedKeys, confirm)}
                    style={{ width: 188, marginBottom: 8, display: 'block' }}
                />
                <Button
                    type="primary"
                    onClick={() => handleSearch(selectedKeys, confirm)}
                    icon="search"
                    size="small"
                    style={{ width: 90, marginRight: 8 }}
                >
                    Search
                </Button>
                <Button onClick={() => handleReset(clearFilters)} size="small" style={{ width: 90 }}>
                    Reset
                </Button>
            </div>
        ),

        filterIcon: filtered => (
            <Icon type="search" style={{ color: filtered ? '#1890ff' : undefined }} />
        ),

        onFilter: (value, record) =>
            record[dataIndex]
                .toString()
                .toLowerCase()
                .includes(value.toLowerCase()),

        onFilterDropdownVisibleChange: visible => {
            if (visible) {
                setTimeout(() => console.log("method called"));
            }
        },

        render: text => (
            <Highlighter
                highlightStyle={{ backgroundColor: '#ffc069', padding: 0 }}
                searchWords={[searchText]}
                autoEscape
                textToHighlight={text.toString()}
            />
        ),
    });

    const handleSearch = (selectedKeys, confirm) => {
        confirm();
        setSearchText(selectedKeys[0]);
    };

    const handleReset = clearFilters => {
        clearFilters();
        setSearchText('');
    };

    const columns = [
        {
            title: "Id",
            dataIndex: "_id",
            key: "active",
            width: "20%",
        },
        {
            title: 'Total Cost',
            dataIndex: 'totalCost',
            key: 'totalCost',
            width: '20%',
            ...getColumnSearchProps('totalCost'),
        },
        {
            title: 'Actual cost',
            dataIndex: 'actualCost',
            key: 'actualCost',
            width: '20%',
            ...getColumnSearchProps('actualCost'),
        },
        {
            title: 'Action',
            dataIndex: '',
            key: 'x',
            render: (e) => (
                <Fragment>
                    <Link to={{ pathname: ORDER_INFO_PATH, orderData: e }}>
                        <Icon type="eye" />
                    </Link>
                    &nbsp;&nbsp;
                </Fragment>)
        },
    ];


    return { columns };
}

// rowSelection objects indicates the need for row selection
const rowSelection = {
    onChange: (selectedRowKeys, selectedRows) => {
        console.log(`selectedRowKeys: ${selectedRowKeys}`, 'selectedRows: ', selectedRows);
    },
    onSelect: (record, selected, selectedRows) => {
        console.log(record, selected, selectedRows);
    },
    onSelectAll: (selected, selectedRows, changeRows) => {
        console.log(selected, selectedRows, changeRows);
    },
};


export default Orders;