import React, { Component } from "react";
import Button from "@material-ui/core/Button";

class CategoryList extends Component {
	render() {
		const props = this.props;
		return (
			<div
				style={{
					height: "400px",
					width: "200px",
					float: "left"
				}}
			>
				<ul>
					{props.group.map((item, i) => {
						let category = props.categoryString;
						let bgColor =
							category.indexOf(item) > -1 ? "#4286f4" : "whitesmoke";
						return (
							<li
								key={i}
								style={{
									background: bgColor,
									borderBottom: "1px solid #afafaf",
									listStyleType: "none",
									textAlign: "left",
									cursor: "pointer"
								}}
								onClick={() => props.itemOnClick(item, props.index)}
							>
								{item}
							</li>
						);
					})}
				</ul>
			</div>
		);
	}
}

class CategorySelector extends Component {
	render() {
		const props = this.props;
		let categoryString = "";
		props.categoryString.forEach((category, i) => {
			const next = i + 1 === props.categoryString.length ? "" : "->";
			categoryString += category + next;
		});

		return (
			<div style={{ width: "90%" }}>
				<h4 style={{ textAlign: "left", marginLeft: "20px" }}>
					Select category
        </h4>
				<div
					style={{
						display: "flex",
						background: "whitesmoke",
						height: "400px",
						width: "100%",
						overflow: "auto"
					}}
				>
					{props.categoryGroups.map((group, i) => (
						<CategoryList
							key={i}
							group={group}
							itemOnClick={props.itemOnClick}
							categoryString={props.categoryString}
							index={i}
						/>
					))}
				</div>
				<p style={{ float: "left" }}>{categoryString}</p>
				<br />

				{props.categoryValidity ? (
					<Button
						variant="contained"
						color="primary"
						style={{ float: "right", marginTop: "20px" }}
						onClick={props.nextButtonAction}
					>
						Next
          </Button>
				) : (
						<Button
							variant="outlined"
							disabled
							style={{ float: "right", marginTop: "20px" }}
						>
							Next
          </Button>
					)}
			</div>
		);
	}
}

export default CategorySelector;
