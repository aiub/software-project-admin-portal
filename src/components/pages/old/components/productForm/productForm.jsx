import React, { Component } from "react";
import Input from "../UI/Input/input";
import Button from "@material-ui/core/Button";
import classes from "./productForm.module.css";
import Spinner from "../UI/Spinner/Spinner";
import productForm from "./data/productFromData";
import productVariantForm from "../productVariantForm/data";
import ProductVariantForm from "../productVariantForm/productVariantForm";
import _ from "lodash";

import axios from "axios";
import { ADD_PRODUCT_PATH } from "../../../../../routes/Slugs";
import { POST_PRODUCT_API } from "../../../../../helpers/apis";
import ConfirmationModal from "../../../../common/ConfirmationModal";


class ProductForm extends Component {
	state = {
		productForm: { ...productForm },
		productVariantForm: productVariantForm,
		productVariantArray: [],
		tag: "",
		tags: [],
		formIsValid: false,
		variantFormIsValid: false,
		loading: false,
		percentageWarning: "",
		amountWarning: ""
	};

	productForm = { ...productForm };

	componentDidMount() {
		if (!this.props.productInfo) {
			let productForm = { ...this.state.productForm };
			for (let p in productForm) {
				if (productForm[p].elementType === "checkBox") {
					productForm[p].config.value = [];
				} else productForm[p].config.value = "";
			}

			this.setState({ productForm });

			return;
		}
		const tags = this.props.productInfo.tags
			? this.props.productInfo.tags.map(t => t.tagId.value)
			: [];

		let productForm = { ...this.state.productForm };
		let productInfo = this.props.productInfo;
		for (let p in productForm) {
			let value =
				productInfo[p] || productInfo[p] === 0
					? productInfo[p]
					: productForm[p].config.value;
			productForm[p].config.value = value;
			productForm[p].valid = true;
		}

		//fill the checkboxes
		// for (let p in productForm) {
		//   if (productForm[p].elementType === "checkBox") {
		//     let options = productForm[p].elementConfig.options.map(e => {
		//       productInfo[p].forEach(p => {
		//         if (p === e.value) {
		//           e.checked = true;
		//           return;
		//         }
		//       });
		//       return e;
		//     });
		//     productForm[p].elementConfig.options = options;
		//     break;
		//   }
		// }
		this.setState({ productForm, tags });
	}

	tagInputChangeHandle = event => {
		this.setState({ tag: event.target.value });
	};

	addTagsHandle = () => {
		if (this.state.tag.length > 0) {
			let tags = [...this.state.tags];
			if (tags.includes(this.state.tag)) return;
			tags.push(this.state.tag);
			this.setState({ tags: tags, tag: "" });
		}
	};

	handleKeyPress = event => {
		if (event.charCode == 13) {
			event.preventDefault();
			this.addTagsHandle(event);
		}
	};

	deleteTagHandle = (event, tag) => {
		event.preventDefault();
		let tags = [...this.state.tags];
		tags = tags.filter(e => e != tag);
		this.setState({ tags });
	};

	updateProduct = () => {
		this.setState({ loading: true });
		let formData = new FormData();
		let i = 0;
		for (let formElementIdentifier in this.state.productForm) {
			let value = this.state.productForm[formElementIdentifier].config.value;
			if (value || value === 0)
				formData.append(
					formElementIdentifier,
					this.state.productForm[formElementIdentifier].config.value
				);
			i++;
		}

		this.state.productForm.thumbnailImages.config.value.forEach(image => {
			formData.append("thumbnailImages", image);
		});

		//add department and category
		formData.append("department", this.props.department);
		formData.append("category", JSON.stringify(this.props.categoryString));

		formData.delete("warehouseAddresses");
		formData.append(
			"warehouseAddresses",
			JSON.stringify(this.state.productForm.warehouseAddresses.config.value)
		);

		formData.append("tags", JSON.stringify(this.state.tags));

		axios
			.put(
				`api/v1/products/` +
				this.props.productInfo.productId,
				formData,
				{
					headers: {
						"Content-Type": "multipart/form-data"
					}
				}
			)
			.then(response => {
				console.log(response.data);
				this.setState({ loading: false });
			})
			.catch(error => {
				console.log(error);
				this.setState({ loading: false });
			});
	};

	productHandler = event => {
		event.preventDefault();
		console.log("before->", this.state.productVariantArray);
		if (this.props.updatingProduct) {
			this.updateProduct();
			return;
		}
		this.setState({ loading: true });
		let formData = new FormData();
		let i = 0;
		for (let formElementIdentifier in this.state.productForm) {
			let value = this.state.productForm[formElementIdentifier].config.value;
			if (value || value === 0)
				formData.append(
					formElementIdentifier,
					this.state.productForm[formElementIdentifier].config.value
				);
			i++;
		}

		// for price range filed
		let min = this.state.productVariantArray[0].price;
		let max = min;
		let totalStock = 0;
		this.state.productVariantArray.forEach(p => {
			if (p.price > max) max = p.price;
			if (p.price < min) min = p.price;
			totalStock += parseInt(p.quantity);
		});
		formData.append("totalStock", totalStock);
		formData.append("priceRange", JSON.stringify([min, max]));
		this.state.productForm.thumbnailImages.config.value.forEach(image => {
			formData.append("thumbnailImages", image);
		});

		//add department and category
		formData.append("department", this.props.department);
		formData.append(
			"category",
			JSON.stringify(this.props.categoryString)
		);

		formData.delete("warehouseAddresses");
		formData.append(
			"warehouseAddresses",
			JSON.stringify(this.state.productForm.warehouseAddresses.config.value)
		);

		//add Variants to formData

		let images = [];
		this.state.productVariantArray.forEach(variant => {
			images.push(variant.images[0]);
		});
		let variantArray = JSON.parse(
			JSON.stringify([...this.state.productVariantArray])
		);

		//make product variant array suitable for backend data model.
		let productVariants = [];
		let priceValidity = true;
		let priceZeroIsValid = false;

		variantArray.forEach(variant => {
			//check and confirm if price is 0..
			if (variant.price === 0 && !priceZeroIsValid) {
				if (window.confirm("variant price remain 0, is it ok ?")) {
					priceZeroIsValid = true;
				} else {
					priceValidity = false;
				}
			}

			if (!priceValidity) return;

			let salePrice = variant.price;
			if (variant.discountAmount && variant.discountAmount.amount > 0) {
				salePrice = salePrice - variant.discountAmount.amount;
			} else delete variant.discountAmount;
			if (variant.discountPercentage && variant.discountPercentage.amount > 0) {
				salePrice = (variant.price * variant.discountPercentage.amount) / 100;
			} else delete variant.discountPercentage;
			let attrs = _.pick(variant, ["colorFamily", "size"]);
			variant.salePrice = salePrice;

			variant.attrs = attrs;
			delete variant.colorFamily;
			delete variant.size;

			//for package pricing.
			let packagePricing = [];
			if (variant.specialPrice.length > 0) {
				variant.specialPrice.forEach(p => {
					packagePricing.push({ piece: p.piece, price: p.price });
				});
				variant.packagePricing = packagePricing;
			} else delete variant.packagePricing;
			productVariants.push(variant);
		});

		if (!priceValidity) {
			window.confirm(
				"product is not saved. Edit product variant price and add product againt"
			);
			this.setState({ loading: false });
			return;
		}

		images.forEach(image => formData.append("images", image));
		formData.append("variants", JSON.stringify(productVariants));

		formData.append("tags", JSON.stringify(this.state.tags));

		axios
			.post(POST_PRODUCT_API, formData, {
				headers: {
					"Content-Type": "multipart/form-data"
				}
			})
			.then(response => {
				console.log(response.data);
				ConfirmationModal('Product uploaded successfully', 'success');
				this.setState({ loading: false });
			})
			.catch(error => {
				console.log(error.response.data);
				this.setState({
					loading: false
				});
			});
		this.setState({ loading: false });
		console.log("after->", this.state.productVariantArray);
	};

	addPricehandle = (piece, price, index) => {
		if (piece <= 0 || price <= 0) return;
		let specialPrice = { piece: piece, price: price };
		let productVariantArray = [...this.state.productVariantArray];
		productVariantArray = productVariantArray.map((e, i) => {
			if (i == index) {
				e.specialPrice.push(specialPrice);
			}
			return e;
		});
		this.setState({ productVariantArray });
	};

	inputChangedHandler = (value, inputIdentifier) => {
		const updatedProductForm = {
			...this.state.productForm
		};
		const updatedFormElement = {
			...updatedProductForm[inputIdentifier]
		};

		updatedFormElement.config.value = value;
		//check form validity
		let validity = checkValidity(value, updatedFormElement.validation);
		updatedFormElement.valid = validity.valid;
		updatedFormElement.errorMsg = validity.errorMsg;

		updatedFormElement.touched = true;
		updatedProductForm[inputIdentifier] = updatedFormElement;

		this.setState({
			productForm: updatedProductForm,
			formIsValid: formIsValid(updatedProductForm)
		});
	};

	checkboxHandle = (event, inputIdentifier) => {
		const updatedProductForm = {
			...this.state.productForm
		};
		const updatedFormElement = {
			...updatedProductForm[inputIdentifier]
		};

		let options = [...updatedFormElement.config.options];
		options = options.map(option => {
			if (option.value == event.target.value) {
				option.checked = event.target.checked;
			}
			return option;
		});
		updatedFormElement.config.options = options;
		let values = updatedFormElement.config.value;
		if (event.target.checked) values.push(event.target.value);
		else values = values.filter(value => value !== event.target.value);
		values = [...new Set(values)];
		updatedFormElement.config.value = values;
		updatedFormElement.touched = true;
		let validity = checkValidity(
			updatedFormElement.config.value,
			updatedFormElement.validation
		);
		updatedFormElement.valid = validity.valid;
		updatedFormElement.errorMsg = validity.errorMsg;
		updatedProductForm[inputIdentifier] = updatedFormElement;

		this.setState({
			productForm: updatedProductForm,
			formIsValid: formIsValid(updatedProductForm)
		});
	};

	addVariantHandler = productVariantArray => {
		this.setState({ productVariantArray, variantFormIsValid: true }, () =>
			console.log("array", this.state.productVariantArray)
		);
	};

	render() {
		const formElementsArray = createFormData(this.state.productForm);
		// const veriantElementsArray = createFormData(this.state.productVariantForm);
		let form = (
			<form>
				{formElementsArray.map((formElement, i) => {
					if (formElement.label === "Add Tags") {
						const tagBoxClass =
							this.state.tags.length > 0
								? classes.tagList
								: classes.tagListEmpty;
						return (
							<div>
								<div className={classes.addTags}>
									<Input
										key={formElement.id}
										elementType={formElement.elementType}
										config={formElement.config}
										value={this.state.tag}
										valid={formElement.valid}
										shouldValidate={formElement.config.validation}
										touched={formElement.config.touched}
										changed={event => this.tagInputChangeHandle(event)}
										onKeyPress={this.handleKeyPress}
										label={formElement.label}
										buttonOnClick={this.addTagsHandle}
									/>
								</div>
								<ul className={tagBoxClass}>
									{this.state.tags.map((e, i) => (
										<li>
											<div className={classes.tagBox}>
												<p>{e}</p>
												<button
													className={classes.tagDeleteButton}
													onClick={event => this.deleteTagHandle(event, e)}
												>
													<span>X</span>
												</button>
											</div>
										</li>
									))}
								</ul>
							</div>
						);
					}

					return (
						<Input
							key={i}
							elementType={formElement.elementType}
							config={formElement.config}
							label={formElement.label}
							valid={formElement.valid}
							errorMsg={formElement.errorMsg}
							required={formElement.required}
							elementName={formElement.elementName}
							changed={this.inputChangedHandler}
							checkboxHandle={this.checkboxHandle}
						/>
					);
				})}

				{!this.props.updatingProduct && (
					<ProductVariantForm
						productVariantArray={this.state.productVariantArray}
						addVariantHandler={this.addVariantHandler}
						removeLoadedFile={this.removeLoadedFile}
						variantFormIsValid={this.state.variantFormIsValid}
						checkVariantFormIsValid={validity =>
							this.setState({ variantFormIsValid: validity })
						}
					/>
				)}

				<Button
					style={{ marginTop: "30px" }}
					color="primary"
					disabled={
						!(
							this.state.formIsValid &&
							(this.state.variantFormIsValid || this.props.updatingProduct)
						)
					}
					variant="contained"
					onClick={this.productHandler}
				>
					Add Product
        </Button>
			</form>
		);
		if (this.state.loading) {
			form = <Spinner />;
		}
		return (
			<div className={classes.ProductData}>
				<div style={{ textAlign: "left" }}>
					<p>Department : {this.props.department}</p>
					<p>
						Category :{" "}
						{this.props.categoryString.map(
							(category, i) =>
								category +
								(i + 1 < this.props.categoryString.length ? "->" : "")
						)}
					</p>
					<Button
						variant="outlined"
						color="primary"
						onClick={this.props.changeCategoryHandle}
					>
						Change Category
          </Button>
				</div>

				{form}
			</div>
		);
	}
}

const createFormData = data => {
	let elementArray = [];
	for (let element in data) {
		let formElement = {
			label: data[element].label,
			elementType: data[element].elementType,
			config: data[element].config,
			valid: data[element].touched ? data[element].valid : true,
			errorMsg: data[element].errorMsg,
			required: data[element].validation.required.value,
			elementName: element
		};
		elementArray.push(formElement);
	}
	return elementArray;
};

const checkValidity = (value, rules) => {
	if (rules === undefined) {
		return true;
	}

	let isValid = true;
	// if (!rules) {
	//   return true;
	// }.
	let errorMsg = "";

	if (
		rules.required.value &&
		rules.type.value !== "file" &&
		rules.type.value !== "array"
	) {
		isValid = value.trim() !== "" && isValid;
		errorMsg += isValid ? "" : rules.required.msg;
		if (!isValid) return { valid: isValid, errorMsg: errorMsg };
	}

	if (rules.type.value === "file" && rules.required.value) {
		isValid = value !== null;
		errorMsg += isValid ? "" : rules.required.msg;
		// return { valid: isValid, errorMsg: errorMsg };
	}

	if (rules.minlength) {
		isValid = value.length >= rules.minlength.value && isValid;
		errorMsg +=
			value.length < rules.minlength.value ? rules.minlength.msg + "" : "";
	}

	if (rules.maxlength) {
		isValid = value.length <= rules.maxlength.value && isValid;
		errorMsg +=
			value.length > rules.maxlength.value ? rules.maxlength.msg + "" : "";
	}

	if (rules.type.value === Number) {
		// const pattern = /^\d+$/;
		// isValid = pattern.test(value) && isValid;
		isValid = value !== NaN;
		errorMsg += isValid ? "" : rules.type.msg;
		if (isValid) {
			isValid = value >= rules.min.value && value <= rules.max.value;
			errorMsg += value >= rules.min.value ? "" : rules.min.msg;
			errorMsg += value <= rules.max.value ? "" : rules.max.msg;
		}
	}
	//console.log(rules);
	if (rules.type.value === Array && rules.required.value) {
		isValid = value.length > 0 && isValid;
	}

	return { valid: isValid, errorMsg: errorMsg };
};

const formIsValid = form => {
	let isValid = true;

	for (let e in form) {
		if (form[e].separetor === undefined) {
			console.log(form[e].valid);
			if (form[e].valid === false) {
				isValid = false;
				break;
			}
		}
	}

	return isValid;
};

export default ProductForm;
