import React, { Component } from "react";
import ProductForm from "../components/productForm/productForm";
import CategorySelector from "../components/productForm/categorySelector";
import { categoryData, departments } from "./categoryData";
// import axios from "axios";
// import { host, port } from "../../address";

class AddProduct extends Component {
	state = {
		products: [],
		selectedDepartment: "",
		categoryGroups: [departments],
		categoryString: [],
		categoryCompleted: false,
		categoryValidity: false
	};

	itemOnClick = (item, index) => {
		let categoryString = this.state.categoryString;
		let categoryGroups = [...this.state.categoryGroups];

		if (categoryString.length > index) {
			let t = categoryData[this.state.selectedDepartment][item] ? true : false;
			categoryGroups = [...categoryGroups.splice(0, index + 1)];
			categoryString = [...categoryString.splice(0, index)];
			this.setState({ categoryValidity: false });
		}

		categoryString.push(item);
		let selectedDepartment = this.state.selectedDepartment;
		selectedDepartment =
			selectedDepartment && index > 0 ? selectedDepartment : item;
		this.setState({ categoryString, selectedDepartment }, () => {
			let group = categoryData[this.state.selectedDepartment][item];
			group && categoryGroups.push(group);
			this.setState({ categoryGroups });

			!group && this.setState({ categoryValidity: true });
		});
	};

	nextButtonAction = () => {
		this.setState({ categoryCompleted: true });
	};

	changeCategoryHandle = () => {
		console.log("press");
		this.setState({ categoryCompleted: false });
	};

	// componentDidMount() {
	// 	axios
	// 		.get(`http://${host}:${port}/api/product`)
	// 		.then(res => {
	// 			this.setState({ products: res.data });
	// 			console.log(res.data);
	// 		})
	// 		.catch(error => {
	// 			this.setState({ loading: false });
	// 		});
	// }

	render() {
		return (
			<div>
				<h2
					style={{
						textAlign: "left",
						color: "rgb(61, 61, 61)",
						padding: "20px",
						fontSize: "17px"
					}}
				>
					POST YOUR PRODUCT
        </h2>
				{this.state.categoryCompleted ? (
					<ProductForm
						department={this.state.categoryString[0]}
						categoryString={this.state.categoryString.slice(1)}
						changeCategoryHandle={this.changeCategoryHandle}
						updatingProduct={false}
					/>
				) : (
						<CategorySelector
							itemOnClick={this.itemOnClick}
							categoryString={this.state.categoryString}
							categoryGroups={this.state.categoryGroups}
							categoryValidity={this.state.categoryValidity}
							nextButtonAction={this.nextButtonAction}
						/>
					)}
			</div>
		);
	}
}

export default AddProduct;
