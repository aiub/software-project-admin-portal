export const departments = [
	"Men",
	"Women",
	"Baby",
	"Gadgets",
	"Electronics",
	"Home_Appliances",
	"Agriculture",
	"Food",
	"Health_and_Beauty",
	"Sports_and_Fitness",
	"Books_and_stationaries",
	"Tools_and_Machinery",
	"Auto_and_vehicles",
	"Institutional_Equipments",
];
export const categoryData = {
	//electronics starts
	Electronics: {
		Electronics: [
			"phone",
			"Computer",
			"Camera",
			"Tv_and_Home_Cinema",
			"Audio_and_SoundSystem"
		],
		phone:
			[
				"Feature",
				"Lan",
				"smart",
				"tablet",
				"walkie_talkie"
			],
		Feature: [
			"brand",
			"non_brand"
		],
		Lan: [
			"brand",
			"non_brand"
		],
		tablet: [
			"brand",
			"non_brand"
		],
		smart: [
			"brand",
			"non_brand"
		],
		walkie_talkie: [
			"brand",
			"non_brand"

		],
		non_brand: [
			"Itel",
			"Tecno",
			"Remax",
			"Symphony",
			"lava",
			"meizu",
			"infinix",
			"okapia",
			"helio",
			"others"
		],
		brand: [
			"Apple",
			"Huawei",
			"Xiaomi",
			"Samsung",
			"Nokia",
			"Google_pixel",
			"Motorola",
			"sony",
			"oneplus",
			"oppo",
			"htc",
			"vivo",
			"Blackberry",
			"others"
		],
		Computer: [
			"Desktop",
			"Laptop",
			"Component"
		],
		"Desktop": [
			"All_in_one",
			"Apple_imac",
			"Apple_imac_mini",
			"Budget",
			"Brand",
			"Gaming",
			"portable_mini"
		],
		"Laptop": [
			"Macbook",
			"Gaming",
			"Primium_ultrabook",
			"budget"

		],
		Component: [
			"Casing",
			"Casing_cooler",
			"Power_supply",
			"Processor",
			"Cpu",
			"Graphics_card",
			"Motherboard",
			"Storage",
			"Ram"
		],
		Storage: [
			"Hard_disk",
			"Portable_hard_disk",
			"SSD",
			"Portable_SSD"
		],
		Ram: [
			"Desktop_ram",
			"laptop_ram"
		],


		Camera: [
			"DSLR",
			"Action",
			"Digital",
			"Handycam",
			"Drones",
			"CCTV",
			"Components",

		],
		Processor: [
			"Intel",
			"Amd"
		],
		Components: [
			"Lens",
			"Flash",
			"Light_modifier",
			"Light_stand",
			"Tripod",
			"Begs",
			"Camera_Case",
			"Memory_card",
			"Betteries",
			"Lens_filter",
			"action_cam_mounts"
		],
		Drones: [
			"Tricopter",
			"Quadcopter",
			"Hexacopter",
			"Octocopter",
			"others"
		],
		CCTV: [
			"Hidden",
			"Wireless",
			"360_camera",
			"body_camera"
		],
		Tv_and_Home_Cinema: [
			"Telivisions",
			"Tv_accessories"
		],
		"Telivisions": [
			"Smart_televisions",
			"LED_televisions",
			"LCD_televisions",
			"Oled_televisions",
			"Crt_televisions"
		],
		Tv_accessories: [
			"Tv_receivers",
			"Tv_remote_controllers",
			"Cable",
			"Wall_mounts",
			"Tv_adaptor"
		],
		Audio_and_SoundSystem: [
			"Headphones",
			"Multimedia_Speaker",
			"Micrphone",
			"Components"
		],
		Headphones: [
			"Mono_headphones",
			"Stereo_headset",
			"In_ear_headphone",
			"Over_the_ear_headphone",
			"Mp3_and_mp4"

		],
		Multimedia_Speaker: [
			"Soundbar",
			"Home_theater",
			"box_types"

		],
		Micrphone: [
			"karaoke",
			"others"
		],
		Components: [
			"wireless_earbuds"
		],
		"Mono_headphones": [
			"AudioType",
		],
		"Stereo_headset": [
			"AudioType",
		],
		In_ear_headphone: [
			"AudioType",
		],
		Over_the_ear_headphone: [
			"AudioType",
		],
		Mp3_and_mp4: [
			"AudioType",
		],
		Soundbar: [
			"AudioType",
		],
		Home_theater: [
			"AudioType",
		],
		box_types: [
			"AudioType",
		],
		karaoke: [
			"AudioType",
		],
		others: [
			"AudioType",
		],
		AudioType: [
			"Wired",
			"Wireless"
		]







	},
	//electronics End
	//Man start
	Men: {
		Men: [
			"Clothing",
			"Watches",
			"Accessories",
			"Shoe",
			"Fragrance",
			"Grooming"

		],
		Clothing: [
			"Shirts",
			"Tshirts",
			"Fotua_and_Panjabi",
			"Pant_and_Trouser",
			"Hoodies_and_jackets",
			"Undergarments",
			"Trench_coats",
			"Suits_Ties_and_Blazer",
			"Novelty_and_special_use"
		],


		Undergarments: [
			"underwares",
			"sleeveless_tshirt"
		],
		Hoodies_and_jackets: [
			"Street_style_hoodies",
			"Colorful_hoodies",
			"Bomber_jackets",
			"Coach_jackets",
			"ski_jackets",
			"Sweaters",
			"others"
		],
		Fotua_and_Panjabi: [
			"Printed_panjabi",
			"Casual_panjabi",
			"Slim_fit_panjabi",
			"Casual_Fotua",
			"Traditional_Fotua",
			"others"
		],
		Pant_and_Trouser: [
			"Jeans",
			"Casual",
			"Joggers_and_Sweatpants",
		],
		Jeans: [
			"Ripped_jeans",
			"Casual_jeans"
		],
		Casual: [
			"Official",
			"gabardine"
		],
		Ripped_jeans: [
			"pant_type"
		],
		Casual_jeans: [
			"pant_type"
		],
		Joggers_and_Sweatpants: [
			"pant_type"
		],
		pant_type: [
			"Full",
			"Half"
		],
		Shirts: [
			"Formal",
			"Casual",
			"Printed",
			"Stripe",
			"Others"
		],
		Casual: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Printed: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Stripe: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Others: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Tshirts: [
			"Printed",
			"Hip_hop",
			"Polo",
			"Round_collar",
			"Casual",
			"Other"
		],
		Printed: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Hip_hop: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Polo: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Round_collar: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Casual: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],
		Other: [
			"Half_sleeve_tshirts",
			"Full_sleeve_tshirts"
		],

		Watches: [
			"Digital",
			"Analog"
		],
		Accessories: [
			"Wallets_and_card_holder",
			"belt",
			"Bagpack",
			"Cap",
			"Fashion_accessories"
		],
		Bagpack: [
			"Basic",
			"Anti_theft",
			"Laptop",
			"Hiking",
			"Travel",
			"Wheeled",
			"Large"
		],
		Wallets_and_card_holder: [
			"Leather_walllet",
			"simple_wallet",
		],

		Shoe: [
			"Casual",
			"Formal",
			"Sandals",
			"Slider",
			"Sneakers",
			"Loafer",
			"Boots",
			"Flip_flop",
			"Footware_Accessories"
		],
		Footware_Accessories: [
			"Socks",
			"Gliters",
			"Others"
		],
		Fragrance: [
			"Perfume",
			"Dudorants"
		],
		Grooming: [

		],
		Novelty_and_special_use: [
			"Wedding_and_event",
			"Hospital",
			"Military",
			"School",
			"Gaurd",
			"Promotional",
			"Hotel_Restaurant",
			"Airline",
			"Fireman",
			"Performance_wear",
			"Tv_and_movie_costumes",
		],
		Wedding_and_event: [
			"Sherwani",
			"Pagri",
		]
	},
	//man ENd
	//women start
	Women: {
		Women: [
			"Clothing",
			"Ladies_Undergarments",
			"Watches",
			"Accessories",
			"Footwear",
			"Fragrance",
			"Bag_and_Clutches",
		],
		Clothing: [
			"Traditional",
			"Western",
		],
		Traditional: [
			"Sharee",
			"Salwar_kamiz_and_kurties",
			"Blouses_and_Tops",
			"Hejabs",
			"Pajamas"
		],
		Salwar_kamiz_and_kurties: [
			"Stitch",
			"Unstitch"
		],
		Western: [
			"Casual_dresses",
			"Hoodies_and_sweatshirts",
			"Leggings",
			"T-shirt",
			"ladies_Pant_and_trouser",
			"Maxi_dresses",
			"Floral_dresses",
			"Cold_shoulder",
		],
		Watches: [
			"Digital",
			"Analog"
		],
		Ladies_Undergarments: [
			"Bra_and_Brief_sets",
			"Panties",

		],
		Fragrance: [
			"Perfume",
			"uni_Perfume",
			"Dudorants"
		],
		Accessories: [
			"Jewellery",
		],
		Jewellery: [
			"Nackless_and_pandants",
			"Earrings",
			"Bangles",
			"Rings"
		],
		Footwear: [
			"pump_shoe",
			"Sandal",
			"flats_sandal",
			"Ballerina",
			"heel",
			"casual_shoe",
		],
		heel: [
			"slim_heel",
			"wedges_heel",
			"block_heel",
			"balance_heel",
		],
		Bag_and_Clutches: [
			"Clutches",
			"ladies_purse",
			"ladies_handbag",
			"Fashionable_Backpacks",
			"Mini_Backpacks",
			"Anti_theft_Backpack",
			"Laptop_backpack",
			"Hiking_backpack",
			"Travel_backpack",
			"Wheeled_backpack",

		],
		Clutches: [
			"ladies_bag_types"
		],
		ladies_purse: [
			"ladies_bag_types"
		],
		ladies_handbag: [
			"ladies_bag_types"
		],
		ladies_bag_types: [
			"Casual",
			"Party",
			"Gorgeous",
			"Occasional",
			"traditional"
		],
		Novelty_and_special_use: [
			"Wedding_and_event",
			"Hospital",
			"Military",
			"School",
			"Gaurd",
			"Promotional",
			"Hotel_Restaurant",
			"Airline",
			"Fireman",
			"Performance_wear",
			"Tv_and_movie_costumes",
		],
		Wedding_and_event: [
			"Lahanga",
			"Gown",
			"Crown",
		]
	},
	//women end
	//baby start
	Baby: {
		Baby: [
			"Clothing",
			"Games_and_toys",
			"Baby_gears",
			"Baby_foods",
			"Diapering",
			"Baby_personal_care",

		],
		Clothing: [
			"Baby_girls",
			"Baby_boys",
			"New_born_unisex",
			"New_born_sets_pack",

		],
		Baby_boys: [
			"0_neck_tshirt",
			"polo_neck_tshirt",
			"0_neck_hoodies",
			"Pullover_hoodies",
			"shorts",
			"Trousers",
			"Shirts",
			"Shoes",
			"Jumpsuits",
			"others"
		],
		Baby_girls: [

			"0_neck_tshirt",
			"polo_neck_tshirt",
			"Frock",
			"Short_sleeve_set",
			"0_neck_hoodies",
			"Pullover_hoodies",
			"Eco_friendly_jacket",
			"Trouser",
			"Jumpsuits",
			"Party_dress",
			"shoes",
			"others"
		],
		Frock: [
			"Printed_Frock",
			"simple_Frock",
			"others"
		],



		Games_and_toys: [
			"action_figures",
			"Block_building_toys",
			"remote_control_toys",
			"dolls",
			"cricket_accessories",
			"football_accessories"
		],
		cricket_accessories: [
			"Bat",
			"ball",
			"stamps",
			"pads",
			"gloves"
		],
		football_accessories: [
			"football",
			"boots",
			"gloves"
		],
		Baby_gears: [
			"baby_walker",
			"swing_jumper",
			"bouncer",
			"baby_cars",
			"backpack_and_carrier"
		],
		Baby_foods: [

		],
		Diapering: [
			"cloth_diapers",
			"wiper",
			"diapar_begs"
		],
		Baby_personal_care: [
			"baby_bath",
			"bathing_tubes_and_seats",
			"Shampoo_and_conditionar",
			"Soaps_and_cleaner"
		]
	},
	//baby end

	//Gadgets starts
	Gadgets: {
		Gadgets: [
			"Smart_watch",
			"PowerBank",
			"VR_box",
			"Memory_card",
			"Graphics_tablet",
			"Rechargable_and_usb_fan",
			"Others"
		]
	},
	//Gadgets end
	//Home_Appliances start
	Home_Appliances: {
		Home_Appliances: [
			"Kitchen_appliances",
			"Large_appliances",
			"Cooling_and_heating",
			"Water_purifier_and_filter",
			"Laundry",
			"Household_cleaning",
			"Home_decor",
			"Furnite",
			"Bath",
			"Bedding",
			"tools",
			"Lighting",
			"stationary_and_craft",
			"Garden_supplies",
			"Bin"

		],
		Kitchen_appliances: [
			"Exhaust_fan",
			"Chimney",
			"Rice_cooker",
			"Blander_mixer_grinder",
			"Electric_kattle",
			"Chopper",
			"Fryer",
			"Coffee_machine",
			"Pressure_Cooker",
			"Sandwich_maker",
			"Specialty_cooker",
			"Toasters",
			"Nonestick_cookware",
			"pots_and_bottles",
			"heat_Registor_gloves",
			"kitchen_apron"
		],
		Large_appliances: [
			"Sewing_machine",
			"Refrigerators",
			"Freezers",
			"Washing Machines",
			"Microwave_oven",
			"Electric_oven",

		],
		Cooling_and_heating: [
			"Fan",
			"Air_Conditioner",
			"Air_Coolers",
			"Air_purifier",
			"Air_fragrance_spreader",
			"Water_Heater"
		],
		Water_purifier_and_filter: [
			"Electical",
			"non_electrical"
		],
		Laundry: [
			"Iron",
			"Iron_table",
			"Hanger",
			"Laundry_busket",
			"Laundry_beg"
		],
		Household_cleaning: [
			"Vacuum_cleaners",
			"Vacuum_cleaners",
			"broom",
			"Mop"
		],
		Home_decor: [
			"flowers_and_plants",
			"candles_and_candles",
			"clocks",
			"Cusions_and_covers",
			"shopies",
			"Carpets",
			"Cartains",

		],
		Furnite: [
			"Bedroom",
			"Diningroom",
			"livingroom",
			"Office"
		],
		Bath: [
			"Towels",
			"Shower_curtains",
			"Body_washer"
		],
		Bedding: [
			"Mattress_pad",
			"Mattress_protector",
			"Pillows",
			"Blankets"
		],
		tools: [
			"Electrical",
			"Plumbing",
			"Hand_tool",
			"Power_tool",
			"Security"
		],
		Lighting: [
			"Celling_light",
			"Floor_lamps",
			"Lamps_shades",
			"Light_bulb",
			"Outdoor_lighting",
			"Table_lamps",
			"Wall_lights"
		],
		stationary_and_craft: [
			"Gift_wrappers",
			"packaging_and_cartton",
			"Paper_products",
			"Art_supplies"
		],
		Garden_supplies: [
			"Grass_cutter",
			"Hand_sprayer"
		]
	},

	//Home_Appliances end
	//Health_and_Beauty start
	Health_and_Beauty: {
		Health_and_Beauty: [
			"Beauty_tools",
			"skin_and_body_care",
			"Fragrance",
			"Hair_cares",
			"Makeup",
			"Mans_care",

			"food_supplements",
			"medical_supplies"
		],
		Beauty_tools: [
			"Curling_iron",
			"Trimar",
			"Hair_dryer",
			"Face_skin_care_tool",
			"Hair_remover_tools",
			"foot_message_tools"
		],
		skin_and_body_care: [
			"Body_message_oil",
			"Mostorizering_cream",
			"Body_scrubs",
			"Body_soap",
			"Shower_gel",
			"Facewash",
			"Handwash",
			"Sun_care_for_body"
		],

		Fragrance: [
			"Mans_fragrance",
			"WoMans_fragrance",
			"Unisex_fragrance"

		],
		Hair_cares: [
			"shampoo",
			"hair_treatment",
			"Conditionar",
			"Hair_brush_and_comb",
			"Hair_color",
			"hair_wigs"
		],
		Makeup: [
			"face",
			"Lips",
			"Eyes",
			"Nails",
			"Hand",
			"makeup_set_and_pallets",
			"Brushes_and_set",
			"Makeup_accessories",
			"Makeup_remover",
		],
		Mans_care: [
			"Deodorents",
			"Shaving_and_grooming",

			"sports_Nutrotions"
		],

		food_supplements: [
			"multivitamins"
		],
		medical_supplies: [
			"Medicine",
			"First_aid_spplies",
			"Health_accessories",
			"Medical_tests",
			"Nebulizers",
			"Wheelchairs"
		]
	},
	//Health_and_Beauty end:

	//Sports_and_Fitness starts
	Sports_and_Fitness: {
		Sports_and_Fitness: [
			"Sports_equipments",
			"Sportswear",
			"Sports_shoes",
			"Exercise_and_fitness",
		],
		Sports_equipments: [
			"Bat",
			"Ball",
			"stamps",
			"Racket",
			"Nets",
			"fethers",
			"gloves"
		],
		Gloves: [
			"Half_finger",
			"Full-covered"
		],
		Swimwears: [
			"Plus_size_swimwear",
			"Womens_bow_swimwear",
			"one_shoulder_swimwear",
			"Floral_swimear"
		],
		yoga_wears: [
			"yoga_pants",
			"yoga_bra",

		],
		Cricket_wears: [
			"jursey",
			"others"
		],
		soccer_wears: [
			"jursey",
			"others"
		],
		Sports_shoes: [

		],
		Exercise_and_Fitness: [
			"Tradmills",
			"cycles",
			"Weight_lifting",
			"Jump_ropes",
			"Dance_pad",
			"Gymn_equipments",
			"Pedometers"
		],
		Gymn_equipments: [
			"Barbell",
			"Spinning_bike",
			"Weight_bench",
			"Power_rack",
			"Situp_bench",
			"Squat_rack",
			"leg_press",
			"Chest_press",
			"Shoulder_press",
			"weight_plate"
		],
		weight_plate: [
			"5kg",
			"10kg",
			"15kg",
			"20kg",
			"30kg",
			"40kg",
			"50kg",


		],
		Sportswear: [
			"Shapers",
			"Swimwears",
			"Bikini",
			"yoga_wears",
			"Hiking_wears",
			"running_wears",
			"Basketball_wears",
			"Cricket_wears",
			"soccer_wears",
			"Gloves"

		],
	},
	//Sports_and_Fitness ends
	//Books_and_stationaries start
	Books_and_stationaries: {
		Books_and_stationaries: [
			"School_stationaries",
			"Office_stationaries",
			"Books",
			"Megazine"
		],
		School_stationaries: [
			"Boards",
			"Board_eraser",
			"calender",
			"paper_cutter",
			"pen",
			"Pencels",
			"rubber",
			"ruller",
			"cutter",
			"file_managers",
			"Calculator",
			"Desk_organiser",
			"Notebooks",
			"writing_pads",
			"printer_supplies",
			"binding_supplies"
		],
		Office_stationaries: [
			"Boards",
			"Board_eraser",
			"calender",
			"Marker_pen",
			"paper_cutter",
			"pen",
			"Pencels",
			"rubber",
			"ruller",
			"cutter",
			"file_managers",
			"Calculator",
			"Desk_organiser",
			"Notebooks",
			"writing_pads",
			"printer_supplies",
			"binding_supplies"
		],
		binding_supplies: [
			"stapler",
			"staples",
			"memo_clip",
			"punch_machine",
			"clipboards",

		],
		Books: [
			"book_cover",
			"comies",
			"novels",
			"story",
			"Scincefictional"
		],
		Megazine: [
			"sports_megazine",
			"lifestyle_megazine",
			"cooking_megazine"

		]
	},
	//Books_and_stationaries end
	//Food start
	Food: {
		Food: [
			"Fruits_and_vegetables",
			"Breakfast",
			"Beverages",
			"Meat_and_fish",
			"Snacks",
			"Dairy",
			"Frozen_food",
			"Bread_and_bakery",
			"Baking_needs",

			"Healthy_and_cholesterol ",
			"Groccery_items"
		],
		Fruits_and_vegetables: [
			"Fresh_fruits",
			"Fresh_vegetables"
		],
		Breakfast: [
			"Local_breakfast",
			"Energy_booster",
			"Bins_and_Cereals",
			"Jam_and_spreads"
		],
		Beverages: [
			"Tea",
			"Coffee",
			"Juice",
			"Soft_Drinks",
			"Energy_drinks",
			"Water",
			"Syrups_and_powder_drinks"
		],
		Meat_and_fish: [
			"Frozen_fish",
			"Dried_fish",
			"Fresh_fish",
			"Tofu_and_meat_alternatives",
			"Meat"
		],
		Snacks: [
			"Noodles",
			"Soups",
			"Pasta_and_Macaroni",
			"Candy_and_Chocolate",
			"Local_snacks",
			"Chips",
			"Popcorn_and_nuts",
			"Biscuits",
			"Salad_dressing",
			"Sauces"
		],
		Dairy: [
			"Liquid_and_uht_milk",
			"Butter_and_sour_cream",
			"Cheese",
			"Eggs",
			"Powder_milk_and_cream",
			"Yogurt"
		],
		Frozen_food: [
			"frozen_snacks",
			"canned_food"
		],
		Bread_and_bakery: [
			"Cookies",
			"Bakery_snacks",
			"Breads",
			"Dips_and_spreads",
			"Honey",
			"Cakes"
		],
		Baking_needs: [
			"Nuts_and_dried_fruits",
			"Dessert_mixes",
			"Baking_ingredients",
			"Flour"
		],
		Healthy_food: [
			"Green_tea",
			"Zero_cal_sugar"
		],
		Groccery_items: [
			"Rice",
			"Spices",
			"Oil",
			"Ghee",
			"Ready_mix",
			"Salt_and_sugar",
			"Dal_or_lentil",
			"Special_ingredients",
			"Shemai_and_suji"
		]
	},
	//Food end
	//Tools_and_Machinery start
	Tools_and_Machinery: {
		Tools_and_Machinery: [
			"Material_handling_tools",
			"Power_tools",
			"Tool_sets",
			"Hand_tools",
			"Electrical_tools",




		],
		Material_handling_tools: [
			"Cement_mixer",
			"Clams_and_vices",
			"Cutting_tools",
			"Lab_equipments",
			"Plumbing_tools",

		],
		Power_tools: [
			"Air_blower",
			"Air_compressor", ,
			"Chain_saw",
			"Dril_machine",
			"Hand_blower_machine",
			"Sewing_machine",
			"Laminating_machine",

		],
		Tool_sets: [
			"Blades",
			"Hand_tool_kit",
			"Screw_and_Nut_drivers",

		],
		Hand_tools: [
			"Hammers",
			"Axe"
		],
		Electrical_tools: [
			"GPS_tracker",

		]
	},

	//Tools_and_Machinery end
	//Agriculture start
	Agriculture: {
		Agriculture: [
			"plants",
			"Seeds",
			"Fertilizer",
			"Feeds",
			"Dairy",
		],
		plants: [
			"Flower_plants",
			"Fruit_plants",
			"Harbal_plants",
			"Big_plants"
		],
		Seeds: [
			"Rice",
			"Grain",
			"Flower",
			"Vegetable",
			"Grass",
		],
		Fertilizer: [
			"Organic",
			"Chemical",
			"Fertilizer_with_insecticide",
			"Liquid_fertilizer"
		],
		Feeds: [
			"Fish_feed",
			"Poltry_feed",
			"Cow_feed",
			"Goat_feed",
		],
		Dairy: [
			"Milk",
			"Cheese",
			"Yogurt"
		],
		Milk: [
			"Cow",
			"Goat",
			"Cammel",
			"Buffalo"
		]
	},
	//Agriculture end
	//Auto_and_vehicles start
	Auto_and_vehicles: {
		Auto_and_vehicles: [
			"Car",
			"Motor_Bike",
			"Car_parts",
			"Motor_Bike_parts",
			"Other_vehicles_parts"
		],
		Car: [
			"Private_car",
			"Jeep",
			"Wagon",
			"Racing"
		],
		Motor_Bike: [

		],
		Car_parts: [
			"Car_cover",
			"Seat_cover",
			"Steering_wheels",
			"Floor_mate",
			"Airfreshner",
			"Rims",
			"Spoiler",
			"Sticker",
			"Sound_system"
		],
		Motor_Bike_parts: [
			"Bike_cover",
			"Tool_box",
			"Helmets",
			"Gloves",
			"Body_aurmor",
			"Mudgud",
			"Horn"
		],
		Other_vehicles_parts: [
			"Bus_parts",
			"Truck_parts"
		]
	},
	//Auto_and_vehicles end
	//Institutional_Equipments start
	Institutional_Equipments: {
		Institutional_Equipments: [
			"Education",
			"Industrial",
			"Ofiice",
			"Garments",
			"Religious"
		],
		Education: [
			"Classroom_chairs",
			"Classroom_table",
			"shelves",
			"White_board",
			"White_board_accessories",
			"Bookcases",
			"Notice_board",

		],
		Industrial: [

		],
		Ofiice: [
			"Office_chairs",
			"Office_table",
			"shelves",
			"White_board",
			"White_board_accessories",
			"Filecases",
			"Notice_board",
		],
		Garments: [
			"Tape_Measure",
			"Sewing_Machine",
			"Scissors",
			"Iron",
			"Leaser_cutting_machine",
			"Lace",
			"Rhinestones",
			"Ribbons",
			"Button",
			"Bedges",
			"zippers"
		],
		Religious: [
			"Floor_mats",

		]
	}
	//Institutional_Equipments end
};
