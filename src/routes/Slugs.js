export const ROOT_PATH = '/';
export const DASHBOARD_PATH = `${ROOT_PATH}dashboard`;

export const LOGIN_PATH = `${ROOT_PATH}login`;
export const LOGOUT_PATH = `${ROOT_PATH}logout`;

export const USER_PATH = `${ROOT_PATH}user`

export const PAGE_403_PATH = `${ROOT_PATH}403`;
export const PAGE_404_PATH = `${ROOT_PATH}404`;
export const PAGE_500_PATH = `${ROOT_PATH}500`;
export const PRODUCT_PATH = `${ROOT_PATH}product`;
export const ADD_PRODUCT_PATH = `${ROOT_PATH}add-product`;
export const PRODUCT_INFO_PATH = `${ROOT_PATH}product-info`;
export const EDIT_PATH = `${ROOT_PATH}edit`;
export const ORDER_PATH = `${ROOT_PATH}orders`;
export const ORDER_INFO_PATH = `${ROOT_PATH}order-info`;

//User
export const USER_LIST_VIEW_PATH = `${ROOT_PATH}user-list`;
export const ADD_USER_PATH = `${ROOT_PATH}add-user`;
export const EDIT_USER_PATH = `${ROOT_PATH}edit-user`;
export const USER_VIEW_PATH = `${ROOT_PATH}user-view`;
