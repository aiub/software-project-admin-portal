import { lazy } from 'react';
import { DASHBOARD_PATH, USER_PATH, PRODUCT_PATH, ADD_PRODUCT_PATH, PRODUCT_INFO_PATH, EDIT_PATH, ORDER_PATH, ORDER_INFO_PATH } from './Slugs';

const Dashboard = lazy(() => import('../components/pages/dashboard/Dashboard'));
const UserView = lazy(() => import('../components/pages/users/UserView'));
const Products = lazy(() => import('../components/pages/products/ProductView'));
const AddProduct = lazy(() => import('../components/pages/old/addProduct/addProduct'));
const ProductInfoView = lazy(() => import('../components/pages/productInfo/ProductInfo'));
const Edit = lazy(() => import('../components/pages/edit/Edit'))
const OrderView = lazy(() => import('../components/pages/orders/OrderView'))
const orderedProductView = lazy(() => import("../components/pages/orderInfo/OrderInfo")
)


// User 
const UserListView = lazy(() => import('../components/pages/user/UserListView'));
const AddUser = lazy(() => import('../components/pages/user/AddUser'));
const EditUser = lazy(() => import('../components/pages/user/EditUser'));
const UserView = lazy(() => import('../components/pages/user/UserView'));

const Routes = [
    {
        path: DASHBOARD_PATH,
        exact: true,
        isPrivate: false,
        component: Dashboard
    },
     //User
     {
        path: PATH.USER_LIST_VIEW_PATH,
        exact: true,
        isPrivate: false,
        component: UserListView
    },
    {
        path: PATH.ADD_USER_PATH,
        exact: true,
        isPrivate: false,
        component: AddUser
    },
    {
        path: `${PATH.USER_VIEW_PATH}/:id`,
        exact: true,
        isPrivate: false,
        component: UserView
    },
    {
        path: `${PATH.EDIT_USER_PATH}/:id`,
        exact: true,
        isPrivate: false,
        component: EditUser
    },
    {
        path: PRODUCT_PATH,
        exact: true,
        isPrivate: false,
        component: Products
    },
    {
        path: PRODUCT_INFO_PATH,
        exact: true,
        isPrivate: false,
        component: ProductInfoView
    },
    {
        path: ADD_PRODUCT_PATH,
        exact: true,
        isPrivate: false,
        component: AddProduct
    },
    {
        path: ORDER_PATH,
        exact: true,
        isPrivate: false,
        component: OrderView
    },
    {
        path: ORDER_INFO_PATH,
        exact: true,
        isPrivate: false,
        component: orderedProductView
    },
    {
        path: EDIT_PATH,
        exact: true,
        isPrivate: false,
        component: Edit
    },
    {
        path: USER_PATH,
        exact: true,
        isPrivate: false,
        component: UserView
    },
]

export default Routes;