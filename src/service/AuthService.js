import axios from "axios";
import {CHANGE_PASSWORD_API_URL, LOGIN_API_URL,GET_USER_PROFILE,UPDATE_PROFILE, CHANGE_PASSWORD,UPDATE_PROFILE_API_URL} from "../helpers/Constant";

export default class AuthService{

    static login(data){
        return axios.post(LOGIN_API_URL, data);
    }

    static getProfile() {
        return axios.get(GET_USER_PROFILE, AuthService.getAuthHeader());
    }

    static updateProfile(profile) {
        return axios.put(UPDATE_PROFILE, profile, AuthService.getAuthHeader());
    }

    static changePassword(data) {
        return axios.put(CHANGE_PASSWORD, data, AuthService.getAuthHeader());
    }

    static getAuthHeader = () => {
        const accessToken = localStorage.getItem("vpn-dis-access-token");
        const options = { headers: { "Authorization": `Bearer ${accessToken}` } }
        return options;
    }

}